// Jan Wennmann
// Nature of Code

let walker;

function setup () {
  // Environment
  createCanvas (400, 400);
  background(0);

  // Variables
  walker = new Walker (width/2, height/2);

}

function draw () {

  walker.show();
  walker.update();

}
