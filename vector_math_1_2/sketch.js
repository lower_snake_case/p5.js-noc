// Jan Wennmann
// Nature of Code

let walker;

function setup () {
  // Environment
  createCanvas (400, 400);

  // Variables
  walker = new Walker (width/2, height/2);

}

function draw () {

  background(0);

  walker.show();
  walker.update();

}
